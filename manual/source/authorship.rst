===========
Authorship
===========

BDSIM was originally started by G.A. Blair around 2001 and has since been
developed and maintained by a group based at Royal Holloway, University of London.

We also welcome contributions and additions to BDSIM. Please contact us to get
involved!

Current Authors
---------------

* Laurie Nevay (*RHUL, lead developer*)
* Stewart Boogert (*RHUL, lead developer*)
* Andrey Abramov (*RHUL*)
* Siobhan Alden (*RHUL*)
* Stephen Gibson (*RHUL*)
* Cedric Hernalsteens(*ULB, CERN*)
* Helena Lefebvre (*RHUL*)
* William Shields (*RHUL*)
* Jochem Snuverink (*PSI, RHUL*)
* Robin Tesse (ULB)
* Stuart Walker (*RHUL*)


Past Authors
------------

* Grahame Blair (*RHUL*)
* Lawrence Deacon (*RHUL, CERN, UCL*)
* Regina Kwee-Hinzmann (*RHUL, CERN*)
* Hector Garcia Morales (*RHUL, CERN*)
* Jaime Van Oers (*RHUL*)
* Will Parker (*RHUL*)
* Yngve Levinsen (*CERN*)
* Giovanni Marchiori (*INFN*)
* Olivier Dadoun (*LAL*)
* Helmut Burkhardt (*CERN*)
* Daniel Brewer (*RHUL*)
* Robert Ainsworth (*RHUL*)
* Stephen Malton (*RHUL*)
* John Carter (*RHUL*)
* Ilya Agapov (*RHUL*)
* Joshua Albrecht (*RHUL*)
