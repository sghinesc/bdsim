#################################
Welcome to BDSIM's documentation!
#################################

.. toctree::
   :maxdepth: 2
   :caption: User Guide

   licence
   authorship
   introduction
   installation
   running
   input_syntax
   model_construction
   model_control
   model_customisation
   model_conversion
   externalgeometry
   output
   output_analysis
   python_utilities
   visualisation
   interfacing
   examples
   support

.. toctree::
   :maxdepth: 1
   :caption: Developer Guide
   
   history
   developer
   model_description


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

