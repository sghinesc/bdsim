/* 
Beam Delivery Simulation (BDSIM) Copyright (C) Royal Holloway, 
University of London 2001 - 2021.

This file is part of BDSIM.

BDSIM is free software: you can redistribute it and/or modify 
it under the terms of the GNU General Public License as published 
by the Free Software Foundation version 3 of the License.

BDSIM is distributed in the hope that it will be useful, but 
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with BDSIM.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "BDSDebug.hh"
#include "BDSGlobalConstants.hh"
#include "BDSMultiSensitiveDetectorOrdered.hh"
#include "BDSRunManager.hh"
#include "BDSSDEnergyDeposition.hh"
#include "BDSSDEnergyDepositionGlobal.hh"
#include "BDSStackingAction.hh"

#include "globals.hh" // geant4 globals / types
#include "G4Run.hh"
#include "G4Event.hh"
#include "G4ThreeVector.hh"
#include "G4Track.hh"
#include "G4TrackStatus.hh"
#include "G4ParticleDefinition.hh"
#include "G4ParticleTypes.hh"
#include "G4VSensitiveDetector.hh"
#include "G4Version.hh"

#include "TString.h"
#include "TObjArray.h"
#include "TObjString.h"
#include "TTree.h"
#include "TNtuple.h"

#if G4VERSION_NUMBER > 1029
#include "G4MultiSensitiveDetector.hh"
#endif

G4double BDSStackingAction::energyKilled = 0;

BDSStackingAction::BDSStackingAction(const BDSGlobalConstants* globals):
volumeScorerPDGids{},
outputVar{0.}{
  killNeutrinos     = globals->KillNeutrinos();
  stopSecondaries   = globals->StopSecondaries();
  maxTracksPerEvent = globals->MaximumTracksPerEvent();
  if (maxTracksPerEvent == 0) // 0 is default -> no action - set maximum possible number
    {maxTracksPerEvent = LONG_MAX;}

  // volume scorer
  volumeScorerFileName = globals->VolumeScorerFileName();
  TString volScorNamesDummy = globals->VolumeScorerName();
  
  //Create a root tree for each volume
  TObjArray *l = volScorNamesDummy.Tokenize(" ");
  for (Int_t iVol = 0; iVol < l->GetEntries(); iVol++){
    TString volume = (static_cast<TObjString*> (l->At(iVol)))->GetString();
    volumeScorerName.push_back(volume);
  }
  
  //Create PDGid vector of particles to be scored in volume
  TString pdgIdsDummy = globals->VolumeScorerPDGid();
  
  TObjArray *lPdg = pdgIdsDummy.Tokenize(" ");
  for (Int_t iPart = 0; iPart < lPdg->GetEntries(); iPart++){
    Int_t pdgId = (static_cast<TObjString*> (lPdg->At(iPart)))->GetString().Atoi();
    volumeScorerPDGids.push_back(pdgId);
  }

  if (volumeScorerName.size() > 0 && volumeScorerPDGids.size() > 0){
    //Create root file for volume scoring
    printf("============ Volume scoring =========================\n");
    printf("Will create volume scoring file: %s\n", volumeScorerFileName.data());
    volumeScorerFile = new TFile(Form("%s", volumeScorerFileName.data()), "RECREATE");
    volumeScorerDir = new TDirectoryFile("NTuple", "NTuple");
    volumeScorerDir->cd();

    volumeScorerNTuple = new TNtuple("Z26328", "Z26328",
                                        "x:y:z:Px:Py:Pz:t:PDGid:Weight:InitX:InitY:InitZ:InitT:EventID");
    printf("PDGids of particles to be scored: ");
    for (size_t iPart = 0; iPart < volumeScorerPDGids.size(); iPart++){
       printf("%d ", volumeScorerPDGids[iPart]);
    }
    printf("\n");
    printf("Volumes in which particles will be scored: ");
    for (size_t iVol = 0; iVol < volumeScorerName.size(); iVol++){
      printf("%s ", volumeScorerName[iVol].Data());
    }
    printf("\n");
    printf("=====================================================\n");
  } 
}

BDSStackingAction::~BDSStackingAction()
{
  volumeScorerName.clear();
  volumeScorerPDGids.clear();
  if (volumeScorerFile){
    volumeScorerDir->cd();
    volumeScorerNTuple->Write();
    volumeScorerFile->Close();

    delete volumeScorerFile;
  }
}

G4ClassificationOfNewTrack BDSStackingAction::ClassifyNewTrack(const G4Track * aTrack)
{
  G4ClassificationOfNewTrack classification = fUrgent;

#ifdef BDSDEBUG 
  G4cout<<"StackingAction: ClassifyNewtrack "<<aTrack->GetTrackID()<<
    " "<<aTrack->GetDefinition()->GetParticleName()<<G4endl;
  G4StackManager* SM = G4EventManager::GetEventManager()->GetStackManager();
  G4cout<<"N total, waiting, urgent, postponed tracks: "
	<< std::setw(6) << SM->GetNTotalTrack()    << " : "
	<< std::setw(6) << SM->GetNWaitingTrack()  << " : "
	<< std::setw(6) << SM->GetNUrgentTrack()   << " : "
	<< std::setw(6) << SM->GetNPostponedTrack()
	<< G4endl;
#endif

  // If beyond max number of tracks, kill it
  if (aTrack->GetTrackID() > maxTracksPerEvent)
    {classification = fKill;}

  // Kill all neutrinos
  if (killNeutrinos)
    {
      G4int pdgNr = std::abs(aTrack->GetParticleDefinition()->GetPDGEncoding());
      if( pdgNr == 12 || pdgNr == 14 || pdgNr == 16)
	{classification = fKill;}
    }

  // kill secondaries
  if (stopSecondaries && (aTrack->GetParentID() > 0))
    {classification = fKill;}

  // volume scoring
  if (volumeScorerName.size() > 0 && volumeScorerPDGids.size() > 0){
    // Get Track starting point volume
    G4int pdgID = aTrack->GetParticleDefinition()->GetPDGEncoding();
    if (std::find(volumeScorerPDGids.begin(), volumeScorerPDGids.end(), pdgID) != volumeScorerPDGids.end()){
      TString volNameRaw = aTrack->GetVolume()->GetName();
      TObjArray *l = volNameRaw.Tokenize("_");
      TString volName = (static_cast<TObjString*> (l->At(0)))->GetString();
      if (std::find(volumeScorerName.begin(), volumeScorerName.end(), volName) != volumeScorerName.end()){
        G4ThreeVector position = aTrack->GetPosition();
        G4ThreeVector momentum = aTrack->GetMomentum();

        outputVar[0] = position.x();
        outputVar[1] = position.y();
        outputVar[2] = position.z();
        outputVar[3] = momentum.x();
        outputVar[4] = momentum.y();
        outputVar[5] = momentum.z();
        outputVar[6] = 0.0; // t
        outputVar[7] = 1.*pdgID;
        outputVar[8] = aTrack->GetWeight();
        outputVar[9] = position.x();
        outputVar[10] = position.y();
        outputVar[11] = position.z();
        outputVar[12] = 0.0; // InitT
        outputVar[13] = 1.*G4RunManager::GetRunManager()->GetCurrentEvent()->GetEventID();

        volumeScorerDir->cd();
        volumeScorerNTuple->Fill(outputVar);
      }
    }
  }
  
  // Here we must take care of energy conservation. If we artificially kill the track
  // we should record its loss as energy deposition. Find if the volume is sensitive
  // and if so record the track there. Note a track is not a step and is a snap shot at
  // one particular point. Therefore, it has a different method in BDSSDEnergyDeposition.
  if (classification == fKill)
    {
      G4VPhysicalVolume* pv = aTrack->GetVolume();
      if (pv)
	{
	  G4VSensitiveDetector* sd = pv->GetLogicalVolume()->GetSensitiveDetector();
	  if (sd) // SD optional attachment to logical volume
	    {
	      if (auto ecSD = dynamic_cast<BDSSDEnergyDeposition*>(sd))
		{ecSD->ProcessHitsTrack(aTrack, nullptr);}
#if G4VERSION_NUMBER > 1029
	      else if (auto mSD = dynamic_cast<G4MultiSensitiveDetector*>(sd))
		{
		  for (G4int i=0; i < (G4int)mSD->GetSize(); ++i)
		    {
		      if (auto ecSD2 = dynamic_cast<BDSSDEnergyDeposition*>(mSD->GetSD(i)))
			{ecSD2->ProcessHitsTrack(aTrack, nullptr);}
		      if (auto egSD = dynamic_cast<BDSSDEnergyDepositionGlobal*>(mSD->GetSD(i)))
			{egSD->ProcessHitsTrack(aTrack, nullptr);}
		      else if (auto mSDO = dynamic_cast<BDSMultiSensitiveDetectorOrdered*>(sd))
			{
			  for (G4int j=0; j < (G4int)mSDO->GetSize(); ++j)
			    {
			      if (auto ecSD3 = dynamic_cast<BDSSDEnergyDeposition*>(mSDO->GetSD(j)))
				{ecSD3->ProcessHitsTrack(aTrack, nullptr);}
			      // else another SD -> don't use -> based on which SDs are constructed with BDSMultiSensitiveDetectorOrdered
			      // in BDSSDManager. e.g. we dont' need BDSSDEnergyDepositionGlobal here
			    }
			  // else another SD -> don't use
			}
		    }
		}
#endif
	      else if (auto mSDO = dynamic_cast<BDSMultiSensitiveDetectorOrdered*>(sd))
		{
		  for (G4int i=0; i < (G4int)mSDO->GetSize(); ++i)
		    {
		      if (auto ecSD2 = dynamic_cast<BDSSDEnergyDeposition*>(mSDO->GetSD(i)))
			{ecSD2->ProcessHitsTrack(aTrack, nullptr);}
		      // else another SD -> don't use
		    }
		}
	      else
		{energyKilled += aTrack->GetTotalEnergy();} // no suitable SD, but add up anyway
	    }
	  else
	    {energyKilled += aTrack->GetTotalEnergy();} // no SD, but add up anyway
	}
      else
	{energyKilled += aTrack->GetTotalEnergy();} // no PV - unusual but possible - add up anyway
    }
  
  return classification;
}

void BDSStackingAction::NewStage()
{
  // urgent stack empty, looking into the waiting stack
#ifdef BDSDEBUG
  G4cout<<"StackingAction: New stage"<<G4endl;
#endif
  return;
}
    
void BDSStackingAction::PrepareNewEvent()
{;}


