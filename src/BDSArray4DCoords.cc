/* 
Beam Delivery Simulation (BDSIM) Copyright (C) Royal Holloway, 
University of London 2001 - 2021.

This file is part of BDSIM.

BDSIM is free software: you can redistribute it and/or modify 
it under the terms of the GNU General Public License as published 
by the Free Software Foundation version 3 of the License.

BDSIM is distributed in the hope that it will be useful, but 
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with BDSIM.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "BDSArray4DCoords.hh"
#include "BDSDebug.hh"
#include "BDSException.hh"
#include "BDSExtent.hh"
#include "BDSFieldValue.hh"
#include "BDSUtilities.hh"

#include "CLHEP/Units/PhysicalConstants.h"

#include <algorithm>
#include <limits>
#include <ostream>
#include <string>

#include "globals.hh"

BDSArray4DCoords::BDSArray4DCoords(G4int nXIn, G4int nYIn, G4int nZIn, G4int nTIn,
				   G4double xMinIn, G4double xMaxIn,
				   G4double yMinIn, G4double yMaxIn,
				   G4double zMinIn, G4double zMaxIn,
				   G4double tMinIn, G4double tMaxIn):
  BDSArray4D(nXIn,nYIn,nZIn,nTIn),
  xMin(xMinIn), xMax(xMaxIn),
  yMin(yMinIn), yMax(yMaxIn),
  zMin(zMinIn), zMax(zMaxIn),
  tMin(tMinIn), tMax(tMaxIn),
  smallestSpatialStep(std::numeric_limits<double>::max())
{
  // There are 1 fewer differences than the points.
  if (nX > 1)
    {
      xStep = (xMax - xMin) / ((G4double)nX - 1);
      CheckStep(xStep, "x");
      smallestSpatialStep = std::min(smallestSpatialStep, xStep);
    }
  else
    {xStep = 1;}
  if (nY > 1)
    {
      yStep = (yMax - yMin) / ((G4double)nY - 1);
      CheckStep(yStep, "y");
      smallestSpatialStep = std::min(smallestSpatialStep, yStep);
    }
  else
    {yStep = 1;}
  if (nZ > 1)
    {
      zStep = (zMax - zMin) / ((G4double)nZ - 1);
      CheckStep(zStep, "z");
      smallestSpatialStep = std::min(smallestSpatialStep, zStep);
    }
  else
    {zStep = 1;}
  if (nT > 1)
    {
      tStep = (tMax - tMin) / ((G4double)nT - 1);
      CheckStep(tStep, "t");
      G4double lengthScale = CLHEP::c_light * tStep;
      smallestSpatialStep = std::min(smallestSpatialStep, lengthScale);
    }
  else
    {tStep = 1;}
}

void BDSArray4DCoords::CheckStep(G4double step, const G4String& name) const
{
  if (!BDS::IsFinite(step))
    {
      throw BDSException(__METHOD_NAME__, "Invalid " + name + "min and " + name + 
			 "max in array leading to 0 step size between points.");
    }
}

G4bool BDSArray4DCoords::OutsideCoords(const G4double x,
				       const G4double y,
				       const G4double z,
				       const G4double t) const
{
  G4bool rx = x < xMin || x > xMax;
  G4bool ry = y < yMin || y > yMax;
  G4bool rz = z < zMin || z > zMax;
  G4bool rt = t < tMin || t > tMax;
  return rx || ry || rz || rt;
}

void BDSArray4DCoords::OutsideCoordsWarn(const G4double x,
					 const G4double y,
					 const G4double z,
					 const G4double t) const
{
  if (OutsideCoords(x,y,z,t))
    {
      throw BDSException(__METHOD_NAME__, "(" +
			 std::to_string(x) + ", " +
			 std::to_string(y) + ", " +
			 std::to_string(z) + ", " +
			 std::to_string(t) + ") is outside array");
    }
}

std::ostream& BDSArray4DCoords::Print(std::ostream& out) const
{
  out << "X: (" << xMin << ", " << xMax << ")" << G4endl;
  out << "Y: (" << yMin << ", " << yMax << ")" << G4endl;
  out << "Z: (" << zMin << ", " << zMax << ")" << G4endl;
  out << "T: (" << tMin << ", " << tMax << ")" << G4endl;
  return BDSArray4D::Print(out);
}

std::ostream& operator<< (std::ostream& out, BDSArray4DCoords const &a)
{
  return a.Print(out);
}

BDSExtent BDSArray4DCoords::Extent() const
{
  return BDSExtent(xMin, xMax, yMin, yMax, zMin, zMax);
}
